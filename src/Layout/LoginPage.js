import * as React from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import { userService } from '../Services/userService';
const theme = createTheme();

const handleSubmit =async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    var value ={
      email: data.get('email'),
      password: data.get('password')
    }
    let $user =await userService.login(value)
    if($user.length>0){
      localStorage.setItem('isLoggedIn',JSON.stringify(true));
      localStorage.setItem('user',JSON.stringify($user));
      window.location.reload();
    }else{
      localStorage.removeItem('user')
    }
  

  };

function LoginPage() {
    return ( 
        <ThemeProvider theme={theme}>
             <Container component="main" maxWidth="xs"
              sx={{
                bgcolor: 'background.paper',
                boxShadow: 1,
                borderRadius: 1,
                p: 2,
                my:10,
               
              }}
              >
            <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
             
              autoFocus
            />
             <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
             
            />
             <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs >
                <Link href="forgetPassword" variant="body2" sx={{float:'left'}}>
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="signUp" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
          </Container>
        </ThemeProvider>



     );
}

export default LoginPage;

