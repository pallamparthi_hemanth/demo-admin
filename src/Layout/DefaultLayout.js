import LoginPage from "./LoginPage";
import SignUp from "./SignUp";
import ForgetPassword from "./ForgetPassword";
import {BrowserRouter,Route} from 'react-router-dom';
function DefaultLayout() {
    return ( <div>
        <BrowserRouter>
        <Route path="/" component={LoginPage} exact/>
        <Route path="/signUp" component={SignUp} exact/>
        <Route path="/forgetPassword" component={ForgetPassword} exact/>
      
     
        </BrowserRouter>
        
      
    </div> );
}

export default DefaultLayout;