import React from 'react';
import DashBoard from '../Pages/HomePage/DashBoard';
import TopBar from './TopBar'
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import ChangePassword from '../Pages/User/changePassword';
import FriendsIndex from '../Pages/Friends/index';
import Profile from '../Pages/User/profile';
import MyCalendar from '../Pages/MyCalendar';
import { Avatar, Card, CardMedia, Divider, Drawer, List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { Box } from '@mui/system';
import { AddBox, Inbox, Mail } from '@mui/icons-material';
import Celebrities from '../Pages/Celebrities';


function LoginLayout() {


  const [open, setOpen] = React.useState(false);

  const handleDrawerClose = (open) => (event) =>
  {
  
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setOpen(false);
  };

  const list = () => (
    <Box
      sx={{ width: 250 }}
      role="presentation"
      onClick={handleDrawerClose(false)}
      onKeyDown={handleDrawerClose(false)}
    >
      <List>
      <Box sx={{ height: 200,padding:2.5,textAlign:'-webkit-center' }}>
      <Avatar  sx={{ width: 150, height: 150 }}  variant="rounded">
        
         <CardMedia
        component="img"
       
        image="https://greenitive-demo.s3.amazonaws.com/no-image.png"
        alt="Image"
      />
     
      </Avatar>
      </Box>
          <Divider />
          <ListItem >
          <ListItemIcon>
               <Inbox /> 
            </ListItemIcon>
          <Link to="/myCalendar" 
           style={{textDecoration:'auto',color:'black'}}>
           
           <ListItemText primary='My Calendar' />
             
         
          </Link>
          </ListItem>
          <ListItem >
          <ListItemIcon>
               <Inbox /> 
            </ListItemIcon>
          <Link to="/celebrities" 
           style={{textDecoration:'auto',color:'black'}}>
           
           <ListItemText primary='Celebrities' />
             
         
          </Link>
          </ListItem>
      {/* <ListItem button href="/friends" >
            <ListItemIcon>
               <Inbox /> 
            </ListItemIcon>
            <ListItemText primary='My Calendar' />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
            <Mail/>
            </ListItemIcon>
            <ListItemText primary='Celebrities' />
          </ListItem> */}
        {/* {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <Inbox /> : <Mail />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))} */}
      </List>
      <Divider />
     
    </Box>
  );

    return ( 
        <div>
            
          
            <BrowserRouter>
            <TopBar
               open={open}
               setOpen={setOpen}
            >
              </TopBar>
        <Switch>
        <Route path="/changePassword">
            <ChangePassword />
          </Route>
          <Route path="/myCalendar">
           <MyCalendar />
          </Route>
          <Route path="/celebrities">
           <Celebrities />
          </Route>
          
          <Route path="/friends">
            <FriendsIndex />
          </Route>
          <Route path="/profile">
            <Profile />
          </Route>
          <Route path="/">
          <DashBoard/>
          </Route>
         
        
        </Switch>
        <Drawer
            anchor='left'
            open={open}
            onClose={handleDrawerClose(false)}
          >
            {list()}
           
          </Drawer>
      </BrowserRouter>
   
           
        </div>
     );
}

export default LoginLayout;