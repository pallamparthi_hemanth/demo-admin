import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Link from '@mui/material/Link';
import { useHistory } from 'react-router-dom';
import { userService } from '../Services/userService';
import Avatar from "@mui/material/Avatar";

 function TopBar(props) {
  const { open,setOpen} = props;
  const history = useHistory();
  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [image, setImage] = React.useState();
  const [userInfo, setUserInfo] = React.useState();

  const getProfile = async() =>{
    let id = JSON.parse(localStorage.getItem("user"))[0].id;
    let $user = await userService.findOne(id);
    setUserInfo($user)
    if($user && $user.image_url != null){
      setImage($user.image_url)
    }
  }

  React.useEffect(()=>{
    getProfile();
  },[])

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

 

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const Logout = () => {
    setAnchorEl(null);
   localStorage.setItem('isLoggedIn',JSON.stringify(false));
   history.push("/");
   //history.push('/login');

    window.location.reload();
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            onClick={handleDrawerOpen}
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div">
            <Link href="/" underline="none" color="white">
              Demo
            </Link>
          </Typography>
          {auth && (
            <Box sx={{ flexGrow: 1, textAlign: "end" }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <Avatar alt="profile picture" src={image}>
                  {userInfo?.first_name.charAt(0)}
                </Avatar>
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem>
                  {" "}
                  <Link href="profile" underline="none" color="black">
                    Profile
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link href="friends" underline="none" color="black">
                    Friends
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link href="changePassword" underline="none" color="black">
                    Change Password
                  </Link>
                </MenuItem>
                <MenuItem onClick={Logout}>Logout</MenuItem>
              </Menu>
            </Box>
          )}
        </Toolbar>
      </AppBar>
      
    </Box>
  );
}
export default TopBar;
