
import Grid from '@mui/material/Grid';
import { useEffect, useState } from "react";
import Controls from '../../components/Controls';
import { useForm, Form } from '../../components/Controls/useForm';
import { Stack } from '@mui/material';
import CardMedia from '@mui/material/CardMedia';
import Card from '@mui/material/Card';
import reactS3 from 'react-s3';
import moment from 'moment';
const initialFValues = {
  first_name: '',
  last_name: "",
  image_url:"",
  dob: moment().format('yyyy-MM-DD') //new Date()

}

const config = {
  bucketName: 'greenitive-demo',
  dirName: 'photos',
  region: 'us-east-1',
  accessKeyId: 'AKIAU2RB7QWEGXGE6BHB',
  secretAccessKey: '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',
}



function CelebritiesAdd(props) {
  const [image, setImage] = useState()
  const[preview,setPreview] = useState()
  const {addOrEdit,recordForEdit} =props;

  
  const {
    values,
    setValues,

    handleInputChange,

  } = useForm(initialFValues);

  const handleSubmit =async e => {
    e.preventDefault()
    if(image!=undefined)
      {
         var result = await reactS3.uploadFile(image, config)
         if(result.location){
         //  alert("if")
          // console.log("result",result)
           console.log(result.location)
           // console.log("values",values)
            values.image_url=result.location
         }
          
      }
    console.log("completed",values)
    addOrEdit(values);
    
  }
  
  


  useEffect(() => {
    if (recordForEdit != null){
      if(recordForEdit.dob!= null)
       recordForEdit.dob=moment(recordForEdit.dob).format('yyyy-MM-DD');
      setValues({
      ...recordForEdit
  })
    }
    
  
}, [recordForEdit]);

function uploadFile(e){
  console.log(e.target.files[0])
  setImage(e.target.files[0])
  const reader =new FileReader();
  reader.onload=()=>{
    console.log("inside")
    if(reader.readyState ===2){
      //console.log(reader.result)
      setPreview(reader.result)
    }
  }
 reader.readAsDataURL(e.target.files[0])
}




  return (
    <Form onSubmit={handleSubmit}>
      <Grid container justifyContent="center">
      <Grid item xs={6} >
      <Stack direction="column" spacing={1} style={{alignItems: 'center'}}>
     
       <Card sx={{ maxWidth: 345 }}>
         <CardMedia
        component="img"
        height="100"
        width="100"
        image={values.image_url || preview || "https://greenitive-demo.s3.amazonaws.com/no-image.png"}
        alt="Image"
      />
      </Card>
      <Controls.Input
            name="image_url"
            label="Image"
            type='file'
            onChange={uploadFile}

          />
          </Stack>
        </Grid>
        <Grid item xs={6}  >
          <Controls.Input
            name="first_name"
            label="First Name"
            value={values.first_name}
            onChange={handleInputChange}
           
          />
          <Controls.Input
            name="last_name"
            label="Last Name"
            value={values.last_name}
            onChange={handleInputChange}
            style={{marginTop:30}}
          />
            <Controls.DatePicker
                name="dob"
                label="D.O.B"
                value={values.dob}
                onChange={handleInputChange}
            />
            <div style={{right:50,textAlign:'center'}}>
          <Controls.Button
            type="submit"
            text="Submit" />
        </div>
        </Grid>
      
      </Grid>
    </Form>
  );
}

export default CelebritiesAdd;