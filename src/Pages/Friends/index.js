import React, { Component, useEffect, useState } from 'react';
import PageHeader from '../../components/PageHeader';
import FriendsAdd from './FriendAdd';
import { PeopleOutlineTwoTone, Search } from '@mui/icons-material';
import { Paper, TableBody, TableCell, TableRow, Toolbar, Grid, Typography, InputAdornment } from '@mui/material';
import useTable from '../../components/Controls/useTable'
import { friendsService } from '../../Services/friendsService';
import Controls from '../../components/Controls';
import AddIcon from '@mui/icons-material/Add';
import Popup from '../../components/Controls/Popup';
import EditOutlinedIcon from '@mui/icons-material/EditOutlined';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import moment from 'moment';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(theme => ({
  pageContent: {
      margin: 40,
      padding: 24
  },
  searchInput: {
      width: '75%'
      
  },
  newButton: {
      position: 'absolute',
      right: '10px'
  }
}))

const headCells = [
  { id: 'id', label: 'Id' },
  { id: 'first_name', label: 'Name' },
  {id:'dob',label:'D.O.B'},
  { id: 'actions', label: 'Actions', disableSorting: true }
];

function FriendsIndex() {
  const [records, setRecords] = useState([]);
  const [openPopup, setOpenPopup] = useState(false)
  const [recordForEdit, setRecordForEdit] = useState(null)
  const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })

  const classes = useStyles();


  const { 
     TblContainer,
     TblHead,
     TblPagination,
     recordsAfterPagingAndSorting
     } = useTable(records, headCells,filterFn);

     const handleSearch = e => {
      let target = e.target;
      setFilterFn({
          fn: items => {
              if (target.value == "")
                  return items;
              else
                  return items.filter(x => x.first_name.toLowerCase().includes(target.value))
          }
      })
  }

     const addOrEdit = async (friends) => {
      // alert("addoredit")
       console.log("friends",friends)
       if(friends.id)
       var result =await friendsService.update(friends)
       else
       var result =await friendsService.create(friends)
       console.log(result)
       getDetails()
       setOpenPopup(false)
     
  }

  const openInPopup = item => {
    setRecordForEdit(item)
    setOpenPopup(true)
}

  useEffect(() => {
    getDetails()
  }, []);


  async function getDetails() {
    let $result = await friendsService.findAll();
    setRecords($result)
  }
  return (
    <>
      <Paper style={{ margin: 15, padding: 20 }}>
       
         <Toolbar style={{marginBottom:4}}>
       
           <Grid container spacing={0}>
           <Grid item spacing={0} xs={2}>
             <Typography variant="h5" color="initial">
             Friends
             </Typography>
            
             </Grid>
             <Grid item spacing={0} xs={8}>
             <Controls.Input
                        placeholder="Search..."
                        className={classes.searchInput}
                        size='small'
                        InputProps={{
                          startAdornment: (<InputAdornment position="start">
                              <Search />
                          </InputAdornment>)
                      }}
                        onChange={handleSearch}
                    />
             </Grid>
             <Grid item spacing={2} xs={2} justifyContent='end'>
             <Controls.Button
                text="Add New"
                variant="outlined"
                startIcon={<AddIcon />}
                onClick={() => { setOpenPopup(true);setRecordForEdit(null);}}
              >
             </Controls.Button>
             </Grid>

          
           
           </Grid>
           </Toolbar>
        <TblContainer>
          <TblHead />
          <TableBody>
            
            {
              recordsAfterPagingAndSorting().map(item => (
                <TableRow key={item.id}>
                  <TableCell>
                    {item.id}
                  </TableCell>
                 
                  <TableCell>
                  <Stack direction="row" spacing={1}>
                  <Avatar
                   alt="Image"
                   src={item.image_url || "https://greenitive-demo.s3.amazonaws.com/no-image.png"}
                   sx={{ width: 24, height: 24 }} 
                  />
                  <Typography variant='subtitle2' color="initial">
                  {item.first_name} {item.last_name}
                  </Typography>
                  
                    </Stack>
                  </TableCell>
                  <TableCell>
                    {item.dob ? moment(item.dob).format('MMMM Do YYYY'):''}
                  </TableCell>
                  <TableCell>
                    <Controls.ActionButton
                        color="primary"
                        onClick={() => { openInPopup(item) }}>
                        <EditOutlinedIcon fontSize="small" />
                    </Controls.ActionButton>
                  
                                    </TableCell>
                </TableRow>
              ))
            }
          </TableBody>
          <TblPagination/>
        </TblContainer>
      </Paper>
      <Popup
      title="New Form"
      openPopup={openPopup}
      setOpenPopup={setOpenPopup}>
          <FriendsAdd
          addOrEdit={addOrEdit}
          recordForEdit={recordForEdit} />
      </Popup>
    </>
  );
}

export default FriendsIndex;
