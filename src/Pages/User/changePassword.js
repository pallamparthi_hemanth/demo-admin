import * as React from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import { userService } from '../../Services/userService';
const theme = createTheme();

const handleSubmit =async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    let $id = JSON.parse(localStorage.getItem('user'))[0].id;
    console.log($id)
    var value ={
      password: data.get('new-password')
    }
    console.log(value)
    let $user =await userService.updatepassword($id,value)
    // if($user.length>0){
    //   localStorage.setItem('isLoggedIn',JSON.stringify(true));
    //   localStorage.setItem('user',JSON.stringify($user));
    //   window.location.reload();
    // }else{
    //   localStorage.removeItem('user')
    // }
  

  };

function ChangePassword() {
    return ( 
        <ThemeProvider theme={theme}>
             <Container component="main" maxWidth="xs"
              sx={{
                bgcolor: 'background.paper',
                boxShadow: 1,
                borderRadius: 1,
                p: 2,
                my:10,
               
              }}
              >
            <Typography component="h1" variant="h5">
            Change Password
          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          {/* <TextField
              margin="normal"
              required
              fullWidth
              name="old-password"
              label="Old Password"
             
              id="old-password"
              autoComplete="current-password"
            /> */}
             <TextField
              margin="normal"
              required
              fullWidth
              name="new-password"
              label="New Password"
             
              id="new-password"
              
            />
             <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
             Change
            </Button>
           
          </Box>
          </Container>
        </ThemeProvider>



     );
}

export default ChangePassword;

