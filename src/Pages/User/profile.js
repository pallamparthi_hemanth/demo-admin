import React, { Component } from "react";
import ReactS3 from "react-s3";
import Container from "@mui/material/Container";
import { Button, TextField, Paper } from "@mui/material";
import reactS3 from "react-s3";
import Avatar from "@mui/material/Avatar";
import Stack from "@mui/material/Stack";
import Grid from "@mui/material/Grid";
import { Box } from "@mui/system";
import { userService } from "../../Services/userService";
import { withStyles } from "@mui/styles";
import { withTheme } from "@emotion/react";


const styles = (theme) => ({
  profile: {
    padding: 20,
    height: "60vh",
    width: "90vh",
    margin: "0 auto",
    overflow: "auto",
  },
  avatar: {
    width: "200px",
    height: "200px",
    margin: "50px auto",
  },
  profilePicture: {
    height: "100%",
  },
  info: {
    margin: "20px",
    color: "blue",
  },
  container: {
    marginTop: "30px",
  },
  btn: {},
});

const config = {
  bucketName: "greenitive-demo",
  region: "us-east-1",
  accessKeyId: "AKIAU2RB7QWEGXGE6BHB",
  secretAccessKey: "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",
};

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      profileUrl: "https://greenitive-demo.s3.amazonaws.com/no-image.png",
      userInfo: {},
      readOnly: true,
      preview: "",
      image: "",
    };
  }

  componentDidMount() {
    //alert("ll")
    this.getProfile();
  }

  async getProfile() {
    let $id = JSON.parse(localStorage.getItem("user"))[0].id;
    let $user = await userService.findOne($id);
    this.setState({ userInfo: $user });
    if ($user.image_url != null) {
      this.setState({ profileUrl: $user.image_url });
    }
  }
  // uploadFile(e) {
  //   console.log(e.target.files[0]);
  //   this.setState((prevState) => ({
  //     userInfo: { ...prevState.userInfo, image_url: e.target.value },
  //   }));
  //   console.log("74", this.state.userInfo);
  //   reactS3
  //     .uploadFile(e.target.files[0], config)
  //     .then((data) => {
  //       console.log(data);
  //       //this.state.profileUrl="https://greenitive-demo.s3.amazonaws.com/Screenshot+(1).png"
  //       this.setState({ profileUrl: data.location });
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }

  uploadFile(e) {
    console.log(e.target.files[0]);
    this.setState({image: e.target.files[0]});
    const reader = new FileReader();
    reader.onload = () => {
      console.log("inside");
      if (reader.readyState === 2) {
        //console.log(reader.result)
        this.setState({ preview: reader.result });
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  async saveUrl() {
    let $id = JSON.parse(localStorage.getItem("user"))[0].id;
    console.log($id);
    var value = {
      image_url: this.state.profileUrl,
    };
    console.log(value);
    let $user = await userService.updatepassword($id, value);
    console.log($user);
  }

  handleUserInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    console.log("name", name, value);
    this.setState((prevState) => ({
      userInfo: {
        ...prevState.userInfo,
        [name]: value,
      },
    }));
  }

  async updateProfile(e) {
    e.preventDefault();
    if (this.state.image != undefined) {
      await reactS3
          .uploadFile(this.state.image, config)
          .then((data) => {
            console.log(data);
            //this.state.profileUrl="https://greenitive-demo.s3.amazonaws.com/Screenshot+(1).png"
            this.setState({ profileUrl: data.location });
          })
          .catch((err) => {
            console.log(err);
          });
    }
    let $id = JSON.parse(localStorage.getItem("user"))[0].id;
    console.log(this.state.userInfo);
    const data = {
      first_name: this.state.userInfo.first_name,
      last_name: this.state.userInfo.last_name,
      email: this.state.userInfo.email,
      phone: this.state.userInfo.phone,
      image_url: this.state.profileUrl,
    };
    let $user = await userService.updatepassword($id, data);
    this.setState({ readOnly: true });
  }


  render() {
    const { classes, theme } = this.props;

    return (
      <Grid className={classes.container} elevation={3}>
        <Paper className={classes.profile}>
          <form onSubmit={this.updateProfile.bind(this)}>
            <Grid item container className={classes.profilePicture}>
              <Grid item xs={6}>
                <Avatar
                  alt="profile picture"
                  // src={this.state.profileUrl}
                  src={
                    this.state.preview ||
                    this.state.profileUrl ||
                    "https://greenitive-demo.s3.amazonaws.com/no-image.png"
                  }
                  className={classes.avatar}
                />
                {!this.state.readOnly && (
                  <TextField
                    margin="normal"
                    // required
                    fullWidth
                    InputLabelProps={{
                      shrink: true,
                    }}
                    name="image_url"
                    label="Image"
                    type="file"
                    id="image-url"
                    onChange={this.uploadFile.bind(this)}
                  />
                )}
              </Grid>
              <Grid xs={6} item>
                <TextField
                  className={classes.info}
                  id="standard-read-only-input"
                  label="First Name"
                  name="first_name"
                  required
                  value={this.state.userInfo.first_name || ""}
                  onChange={this.handleUserInput.bind(this)}
                  InputProps={{
                    readOnly: this.state.readOnly,
                  }}
                  variant="outlined"
                />
                <TextField
                  className={classes.info}
                  id="standard-read-only-input"
                  label="Last Name"
                  name="last_name"
                  required
                  value={this.state.userInfo.last_name || ""}
                  onChange={this.handleUserInput.bind(this)}
                  InputProps={{
                    readOnly: this.state.readOnly,
                  }}
                  variant="outlined"
                />
                <TextField
                  className={classes.info}
                  id="standard-read-only-input"
                  label="Email"
                  name="email"
                  required
                  value={this.state.userInfo.email || ""}
                  onChange={this.handleUserInput.bind(this)}
                  InputProps={{
                    readOnly: this.state.readOnly,
                  }}
                  variant="outlined"
                />
                <TextField
                  className={classes.info}
                  id="standard-read-only-input"
                  label="Phone Number"
                  name="phone"
                  required
                  value={this.state.userInfo.phone || ""}
                  onChange={this.handleUserInput.bind(this)}
                  InputProps={{
                    readOnly: this.state.readOnly,
                  }}
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid item align="center" marginTop={0}>
              {!this.state.readOnly && (
                <Grid>
                  <Button type="submit" variant="contained" color="primary">
                    Save
                  </Button>
                </Grid>
              )}
              {this.state.readOnly && (
                <Grid>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => this.setState({ readOnly: false })}
                  >
                    Edit Profile
                  </Button>
                </Grid>
              )}
            </Grid>
          </form>
        </Paper>
      </Grid>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Profile);
