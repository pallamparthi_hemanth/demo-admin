import React, { Component, useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Paper, CardActionArea, Container, Grid, InputAdornment, Stack, Toolbar } from '@mui/material';
import Popup from '../../components/Controls/Popup';
import MyCalendarAdd from './myCalendarAdd';
import Controls from '../../components/Controls';
import { Search } from '@mui/icons-material';
import AddIcon from '@mui/icons-material/Add';
import { makeStyles } from '@mui/styles';
import { myCalendarService } from '../../Services/myCalendarService';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
    pageContent: {
        margin: 40,
        padding: 24
    },
    searchInput: {
        width: '75%'

    },
    newButton: {
        position: 'absolute',
        right: '10px'
    }
}))


function MyCalendar() {
    const classes = useStyles();
    const [records, setRecords] = useState([]);
    const [openPopup, setOpenPopup] = useState(false)
    const [recordForEdit, setRecordForEdit] = useState(null)
    const [filterFn, setFilterFn] = useState({ fn: items => { return items; } })
  
    const handleSearch = e => {
        let target = e.target;
        setFilterFn({
            fn: items => {
                if (target.value == "")
                    return items;
                else
                    return items.filter(x => x.first_name.toLowerCase().includes(target.value))
            }
        })
    }


    const addOrEdit = async (myCalendar) => {
        // alert("addoredit")
        console.log("myCalendar", myCalendar)
         if(myCalendar.id)
         var result =await myCalendarService.update(myCalendar)
         else
         var result =await myCalendarService.create(myCalendar)
         console.log(result)
         getDetails()
         setOpenPopup(false)

    }

    useEffect(() => {
        getDetails()
      }, []);
    
  async function getDetails() {
    let $result = await myCalendarService.findAll();
    setRecords($result)
  }


    return (
        <>
            <Paper style={{ margin: 15, padding: 20 }}>
                <Toolbar style={{ marginBottom: 4 }}>

                    <Grid container spacing={0}>
                        <Grid item spacing={0} xs={2}>
                            <Typography variant="h5" color="initial">
                                My Calendar
                            </Typography>

                        </Grid>
                        <Grid item spacing={0} xs={8}>
                            <Controls.Input
                                placeholder="Search..."
                                className={classes.searchInput}
                                size='small'
                                InputProps={{
                                    startAdornment: (<InputAdornment position="start">
                                        <Search />
                                    </InputAdornment>)
                                }}
                                onChange={handleSearch}

                            />
                        </Grid>
                        <Grid item spacing={2} xs={2} justifyContent='end'>
                            <Controls.Button
                                text="Add New"
                                variant="outlined"
                                startIcon={<AddIcon />}
                                onClick={() => { setOpenPopup(true); setRecordForEdit(null); }}
                            >
                            </Controls.Button>
                        </Grid>



                    </Grid>
                </Toolbar>
                <Grid container spacing={2}>


                    {
                        filterFn.fn(records).map((item, index) => (
                            <Grid key={index} item xs={3}>
                                <Card sx={{ maxWidth: 300, margin: 2 }}>
                                    <CardActionArea>
                                        <CardMedia
                                            component="img"
                                            height="200"
                                            image={item.image_url}
                                            alt="Img"
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {item.first_name}  {item.last_name}
                                            </Typography>
                                            <Typography variant="body2" color="text.secondary">
                                                D.O.B : {moment(item.dob).format('MMM Do YYYY')}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        ))



                    }
                </Grid>
                <Popup
                    title="New Form"
                    openPopup={openPopup}
                    setOpenPopup={setOpenPopup}>
                    <MyCalendarAdd
                        addOrEdit={addOrEdit}
                        recordForEdit={recordForEdit} />
                </Popup>

            </Paper>
        </>
    );
}

export default MyCalendar;