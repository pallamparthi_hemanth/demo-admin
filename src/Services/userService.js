import { config } from "../Config/config";
import axios from 'axios'
function create(data){
   // console.log("inside Userservice",data)
    return axios.post(config.apiUrl+'/users',data).then((res)=>{
       // console.log(res)
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}
function findOne(id){
    
     return axios.get(config.apiUrl+'/users/'+id).then((res)=>{
         console.log('response',res.data)
         return res.data ? res.data:[];
     })
     .catch((err)=>{
         console.log(err)
     })
 
 }
 function login(data){
    
    return axios.post(config.apiUrl+'/userwithemailandpassword',data).then((res)=>{
        console.log('response',res.data)
        return res.data ? res.data:[];
    })
    .catch((err)=>{
        console.log(err)
    })

}
function resetpassword(data){
    
    return axios.post(config.apiUrl+'/resetpassword',data).then((res)=>{
        console.log('response',res)
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}

 function updatepassword(id,data){
    
    return axios.post(config.apiUrl+'/updatepassword/'+id,data).then((res)=>{
        console.log('response',res)
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}
export const userService = {
    resetpassword,
    create,
    findOne,
    login,
    updatepassword
};