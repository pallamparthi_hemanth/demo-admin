import { config } from "../Config/config";
import axios from 'axios'

function findAll(){
    
    return axios.get(config.apiUrl+'/celebrities').then((res)=>{
        console.log('response',res.data)
        return res.data ? res.data:[];
    })
    .catch((err)=>{
        console.log(err)
    })

}


function create(data){
   
    return axios.post(config.apiUrl+'/celebrities',data).then((res)=>{
        console.log("inside celebrities Create")
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}
function findOne(id){
    
     return axios.get(config.apiUrl+'/celebrities/'+id).then((res)=>{
         console.log('response',res.data)
         return res.data ? res.data:[];
     })
     .catch((err)=>{
         console.log(err)
     })
 
 }

 function update(data){
   
    return axios.post(config.apiUrl+'/celebrities/'+data.id,data).then((res)=>{
       // console.log(res)
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}

export const celebritiesService = {
    findAll,
    create,
    findOne,
    update
  
};