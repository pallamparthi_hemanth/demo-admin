import { config } from "../Config/config";
import axios from 'axios'

function findAll(){
    
    return axios.get(config.apiUrl+'/friends').then((res)=>{
        console.log('response',res.data)
        return res.data ? res.data:[];
    })
    .catch((err)=>{
        console.log(err)
    })

}


function create(data){
   
    return axios.post(config.apiUrl+'/friends',data).then((res)=>{
       // console.log(res)
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}
function findOne(id){
    
     return axios.get(config.apiUrl+'/friends/'+id).then((res)=>{
         console.log('response',res.data)
         return res.data ? res.data:[];
     })
     .catch((err)=>{
         console.log(err)
     })
 
 }

 function update(data){
   
    return axios.post(config.apiUrl+'/friends/'+data.id,data).then((res)=>{
       // console.log(res)
        return res;
    })
    .catch((err)=>{
        console.log(err)
    })

}

export const friendsService = {
    findAll,
    create,
    findOne,
    update
  
};