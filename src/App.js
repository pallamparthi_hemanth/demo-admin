
import { CssBaseline } from '@mui/material';
import './App.css';
import DefaultLayout from './Layout/DefaultLayout';
import LoginLayout from './Layout/LoginLayout';
function App() {
   let isLoggedIn = JSON.parse(localStorage.getItem('isLoggedIn') || false)

  return (
    <div className="App">
   
     {
      (()=>{
        if(isLoggedIn){
        return <LoginLayout/>
        }
        else{
        return <DefaultLayout />
        
        }
      })()
    }
     <CssBaseline />
    </div>
  );
}

export default App;



