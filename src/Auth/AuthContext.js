import React, { Component, createContext } from 'react';

export const AuthProvider = createContext() 
class AuthContext extends React.Component {
   constructor(){
       super()
       this.state={
           isLoggedIn:false
       }
   }

    render() { 
        return <AuthProvider.Provider value={{...this.state}}>
            {this.props.children}
        </AuthProvider.Provider>
    }
}
 
export default AuthContext;