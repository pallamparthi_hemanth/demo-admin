import Input from "./Input";
// import RadioGroup from "./RadioGroup";
// import Select from "./Select";
// import Checkbox from "./Checkbox";
 import DatePicker from "./DatePicker";
import Popup from "./Popup";
 import Button from "./Button";
 import ActionButton from "./ActionButton";

const Controls = {
    Input,
    Popup,
    // RadioGroup,
    // Select,
    // Checkbox,
     DatePicker,
     Button,
     ActionButton
}

export default Controls;