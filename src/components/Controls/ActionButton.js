import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react'



const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 0,
        margin: 4
    },
    secondary: {
        backgroundColor: '#ff610057',
        '& .MuiButton-label': {
            color: '#9c27b0',
        }
    },
    primary: {
        backgroundColor: '#ff610057',
        '& .MuiButton-label': {
            color: '#ee791a',
        }
    },
}))

export default function ActionButton(props) {

    const { color, children, onClick } = props;
    const classes = useStyles();

    return (
        <Button
            className={`${classes.root} ${classes[color]}`}
            onClick={onClick}>
            {children}
        </Button>
    )
}
