import { Dialog, DialogContent, DialogTitle, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react'; 
import Controls from '../Controls'
import CloseIcon from '@mui/icons-material/Close';

const useStyles = makeStyles(theme => ({
    dialogWrapper: {
        padding: 16,
        position: 'absolute',
        top: 40
    },
    dialogTitle: {
        paddingRight: '0px'
    }
}))

function Popup(props) {
    const { title, children, openPopup, setOpenPopup } = props;
    const classes = useStyles();
    return ( 
        <Dialog open={openPopup} maxWidth="md" classes={{ paper: classes.dialogWrapper }}>
<DialogTitle>
 <Grid container>
 <Grid item xs={11}>
 {title}
 </Grid>
 <Grid item xs={1}>
    <Controls.ActionButton
    color="secondary"
    onClick={()=>{setOpenPopup(false)}}>
    <CloseIcon />
    </Controls.ActionButton>
</Grid>
</Grid>

   

</DialogTitle>
<DialogContent dividers>
 {children}
</DialogContent>

       </Dialog>
     );
}

export default Popup;