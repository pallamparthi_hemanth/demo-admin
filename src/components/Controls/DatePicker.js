import * as React from 'react';
import TextField from '@mui/material/TextField';
import moment from 'moment';

export default function DatePicker(props) {

    const { name, label, value, onChange } = props

    const convertToDefEventPara = (name, value) => (
        {

        target: {
            name, value
        }
    }
    )

    return (

        <TextField
            id="date"
            label="Birthday"
            type="date"
            format="yyyy-MM-dd"
            name={name}
            value={value}
            onChange={date =>onChange(convertToDefEventPara(name,moment(date.target.value).format('yyyy-MM-DD')))}
            sx={{ width: 220 }}
            InputLabelProps={{
                shrink: true,
            }}
        />


    );
}

